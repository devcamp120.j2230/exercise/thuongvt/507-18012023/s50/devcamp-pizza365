const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8000;

//middleware static
app.use(express.static(__dirname + '/views'));

// nơi khai báo API
app.get("/",(req,res)=>{
    res.sendFile(path.join(__dirname + "/views/pizza365.html"));
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
